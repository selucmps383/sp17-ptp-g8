import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
    <h1>Hello {{name}}</h1>
    <!-- Add router tags here -->
    
  `,
})
export class AppComponent  { name = 'world'; }

// This goes in the template

    // <router-outlet></router-outlet>
    // <a routerLink="/greet">Greet</a>
    //  | <a href="/">Home</a>
