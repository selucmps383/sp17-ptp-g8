import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppComponent }  from './app.component';
// Import module here

@NgModule({
  imports:      [ 
    BrowserModule,
    FormsModule,
    // Add module to import array here

  ],
  declarations: [ 
    AppComponent,
    // Declare component to make it available here
    
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }

// This needs to go with the actual imports
// import { GreetComponent }  from './greet.component';

// This goes in module imports array
// RouterModule.forRoot([ { path: 'greet', component: GreetComponent } ]),

// This goes in declarations
// GreetComponent