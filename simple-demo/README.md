##_Note: this is a simple demo app based on the [tutorial](https://angular.io/docs/ts/latest/tutorial/) on the Angular site._
##_Look at the [Angular Quickstart repo](https://github.com/angular/quickstart) for the original source code._


## Prerequisites

Node.js and npm are essential to Angular development. 
    
<a href="https://docs.npmjs.com/getting-started/installing-node" target="_blank" title="Installing Node.js and updating npm">
Get it now</a> if it's not already installed on your machine.
 
**Verify that you are running at least node `v4.x.x` and npm `3.x.x`**
by running `node -v` and `npm -v` in a terminal/console window.
Older versions produce errors.

## Running the simple demo

* To run the simple demo, download or clone this repo. 
* Then, navigate to the simple-demo directory, where there is the package.json file. 
* Run the command `npm install`
* Next, run the command `npm start`
* Navigate to `[http://localhost:3000`](http://localhost:3000) in your browser.

## Create a component and a route

This demo is set up to help get a very basic understanding of creating a component and a route.

There are 4 files to modify for this to happen.

* `index.html`
* `greet.component.ts`
* `app.module.ts`
* `app.component`

In each of these files there are commented out code snippets at the end of the page, as well as comments withing the code showing where each snippet should go
