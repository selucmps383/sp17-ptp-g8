# Angular 2 Tutorial #

Check out the sample apps in this repository:

* [Simple app](simple-demo) to create a component and route.
* [Advanced demo](angular-demo) that shows some more complex topics.


## What is Angular?

What Is Angular?

Angular 2 is a rewrite of AngularJS, a JavaScript framework used to create web applications. Instead of being an MVC framework, Angular 2 is component based. Angular 2 uses TypeScript as it's language, a superset of JavaScript. Angular 2 improves on AngularJS by focusing on mobile oriented architecture and mobile performance.

Angular 2 does code generation and templates, and is capable of code splitting when using the Component Router. This makes it so users only load the code required to render the current view.  Angular 2 works very well with the Ionic Framework, NativeScript, and React Native. The Angular CLI is also available as command line tools for quick deployment.


Requirements:

* [Node.js][1]

* [NPM][2]

* [Angular 2 Documentation][3]

[1]: https://nodejs.org/en/
[2]: https://www.sitepoint.com/beginners-guide-node-package-manager/
[3]: https://angular.io/docs/ts/latest/

---------------------------------------------------------------------

## TypeScript


What is TypeScript?

Typescript is a superset of JavaScript. It provides more object-oriented programming concepts to create a more 
powerful and organized code structure. Typescript brings many improvements on JavaScript, but does not completely 
abandon the JavaScript language. JavaScript will compile in any .ts(TypeScript) file and run correctly.

Along with this object-oriented focus, variables in TypeScript posses a specific Datatype(string, boolean, ect) 
instead of using the dynamic var type. This creates a more strict codebase, but also a codebase that is less prone 
to defects, as stated in this [study of programming languages and code quality] done by the University of California. 

![datatypes](http://i66.tinypic.com/67l3cj.png)

Because Typescript is compiled, it has the ability to detect errors while coding. In JavaScript, an error can result 
in a long manual search through each line of code after run-time. With Typescript, errors are exposed immediately, 
and a possible solution is suggested as well.

![error](http://i68.tinypic.com/212vced.png)

[study of programming languages and code quality]:<http://web.cs.ucdavis.edu/~filkov/papers/lang_github.pdf>

---------------------------------------------------------------------------

## Modules

Modules are the containers for both code and declarations. Modules are designed to serve a specific purpose, and are written to be interchangeable.
Modules posses their own scope, meaning that the code within cannot be executed globally unless they are given an export form. To access a module, it must be imported.

The NgModule is the "Angular Module". Its purpose is to tell Angular how to run the module code. It also will make some components of the modules public so that they can be used elsewhere.

![](http://i66.tinypic.com/xdgg06.png)

----------------------------------------------------------------------

## Data Binding

Synchronizating the data model and the view of an application is called Data Binding. This two-way binding ensures that data displayed on the view of an application is true to what is kept in the data model. When data in the model changes, the view changes. And when the view changes, the model changes. An example of Data Binding is here:

```javascript


```

```html


```

-------------------------------------------------------------------

## Components

Components are what everything is made of in Angular 2. They are the main way to build and specify elements and logic on a page, and contain custom elements and attributes. Marking something as a component provides additional metadata as to how to process and use the component. Components are subset of directives in Angular, and hold a template. They must belong to NgModule in order to be usable. An example of a component is here:

```typescript

import { Component } from '@angular/core';

 
@Component({
  selector: 'greet',
  template: `<h1>Hello {{name}}</h1>`,
})
export class GreetComponent  { name = 'World'; }

```

This component is given the selector of 'greet' and the template of 'Hello {{name}}!'. The class named as a component is Greet and the name used in the template is a string equal to 'World'.

----------------------------------------------------------------

## Dependency Injection
## What is Dependency Injection?
>Dependency Injection (DI) is a software design pattern that deals with how components get hold of their dependencies. The Angular injector subsystem is in charge of creating components, resolving their dependencies, and providing them to other components as requested.

## Okay.. So what is it?
>It's a coding pattern in which a class receives its dependencies from external sources rather than creating them itself.

## DI in a Nutshell
![DI](https://media-www-asp.azureedge.net/media/44907/dependency-injection-golf.png?raw=true)

## Practicality of DI?
* Use in Unit Testing
* Instead of referring to global variable for dependency, the component has the dependency injected to itself
* Decoupling definitions of classes

#### Resources
* [DI Image](https://media-www-asp.azureedge.net/media/44907/dependency-injection-golf.png?raw=true)
* [DI Definition](https://docs.angularjs.org/guide/di)
* [More on DI](https://angular.io/docs/ts/latest/guide/dependency-injection.html)

---------------------------------------------------------------

## Routing in Angular

## What is a route?
>"A route is a URL pattern that is mapped to a handler. The handler can be a physical file, such as an .aspx file in a Web Forms application. A handler can also be a class that processes the request, such as a controller in an MVC application."
>--[Microsoft MSDN Website](https://msdn.microsoft.com/en-us/library/cc668201.aspx)

## Basic Routing

### Base Tag
The first step of routing is to set the ```<base href>``` tag. For instance, if the root of your application is in the ```app``` directory, you would set the base tag in ```index.html``` like this: 
```html
<base href="/">
```
This sets the ```app``` directory as the route to be used when the application is determining routes. This also tells the browser how to prefix relative URLs when importing assets.

Read more about the [```<base href>```](https://angular.io/docs/ts/latest/guide/router.html#!#base-href) tag and [```history.pushState()```](https://developer.mozilla.org/en-US/docs/Web/API/History_API#Adding_and_modifying_history_entries) for a more comprehensive explanation of how this works.

### Defining Routes

The most basic way to define routes is in the ```AppModule```. Start by importing the Angular ```RouterModule``` in ```app.module.ts```, which looks like this:

```typescript
import { RouterModule }   from '@angular/router';
```
Once imported, the ```RouterModule``` can be used to add configuration to imports array of ```app.module.ts```.

```typescript
RouterModule.forRoot([
  { path: 'greet', component: GreetComponent }
])
```
This configuration would route a URL like ```example.com/greet``` to a component called ```GreetComponent``` which would then handle that request. To add another route, define another path and component, then add that to the array. 

```typescript
RouterModule.forRoot([
  { path: 'greet', component: GreetComponent },
  { path: 'goodbye', component: GoodbyeComponent }
])
```
A request to ```example.com/goodbye``` would then be handled by the ```GoodbyeComponent```.

_Note: The components will also have to be imported to_ ```app.module```.

### Routing and Views
Routers display views using the ```RouterOutlet``` directive from the ```RouterModule```. This gives us the tag ```<router-outlet>``` which is used as a DOM element in the template of the component the router is mapped to.

```typescript
template:`
    <h1>Title</h1>
    <router-outlet></router-outlet>
    <!-- view would be displayed here in the browser -->
```
### Routing and Links

Using ```RouterLink```, another directive of ```RouterModule```, routes can be bound to an ```<a>``` tag so that routing can be used with links. This also goes in the template of a component.

```typescript
template:`
    <h1>Title</h1>
    <a routerLink="/greet">Greet</a>
`
```
Clicking on the ```Greet``` link would send the request through the 'greet' route to the ```GreetComponent```.

## Advanced Routing

* redirectTo
* route with parameters
* Separating routes into their own file.


## Resources

* [Angular Docs](https://angular.io/docs/ts/latest/guide/router.html "Routing and Navigation")
* [angular-university](http://blog.angular-university.io/angular2-router/ "Angular 2 Router Introduction")
* [vsavkin.com Angular Blog](https://vsavkin.com/angular-2-router-d9e30599f9ea#.2zn84h6pk)

---------------------------------------------------------------

## Pipes

Pipes are what make raw data more appealing to the human eye. Pipes use the "pipe operator" ( | ) to achieve this. 

The raw data variable is entered on to the left side of the pipe operator, while the "Date pipe" function on the right transforms its from raw data.

```javascript
myBirthday = new Date(1994, 6, 9);
.....
.....
.....
<p>My birthday is {{ myBirthday | date }}</p>
}
```

The code above turns the raw string "Thu Jun 9 1994 00:00:00 GMT-0700 (Pacific Daylight Time)" into June 9, 1994.

Along with this raw data conversion, format types are available, such as 'shortDate' and 'fullDate'.

-----------------------------------------------------------------------

## Index ##

* [What Angular Is and Can Do](angular)
* [TypeScript](typescript)
* [Modules](modules)
* [Data Binding](databinding)
* [Components](components)
* [Dependency Injection](dependencyinjection)
* [Routing](routing)
* [Pipes](pipes)
* [Demo App](angular-demo)

## Project Setup

The project repo is currently set up with a separate directory for each topic and an image subdirectory for each topic. Each topic directory has its own README.md file which can be used to keep tutorial topics separated during development. This structure was made to help maintain organization, but can be changed as you see fit.