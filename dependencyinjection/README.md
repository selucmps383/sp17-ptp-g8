# Dependency Injection
## What is Dependency Injection?
>Dependency Injection (DI) is a software design pattern that deals with how components get hold of their dependencies. The Angular injector subsystem is in charge of creating components, resolving their dependencies, and providing them to other components as requested.

## Okay.. So what is it?
>It's a coding pattern in which a class receives its dependencies from external sources rather than creating them itself.

## DI in a Nutshell
![DI](https://media-www-asp.azureedge.net/media/44907/dependency-injection-golf.png?raw=true)

## Practicality of DI?
* Use in Unit Testing
* Instead of referring to global variable for dependency, the component has the dependency injected to itself
* Decoupling definitions of classes

#### Resources
* [DI Image](https://media-www-asp.azureedge.net/media/44907/dependency-injection-golf.png?raw=true)
* [DI Definition](https://docs.angularjs.org/guide/di)
* [More on DI](https://angular.io/docs/ts/latest/guide/dependency-injection.html)


[<-previous](../components) |
[home](../) |
[next->](../pipes)
