# Pipes

Pipes are what make raw data more appealing to the human eye. Pipes use the "pipe operator" ( | ) to achieve this. 

The raw data variable is entered on to the left side of the pipe operator, while the "Date pipe" function on the right transforms its from raw data.

```javascript
myBirthday = new Date(1994, 6, 9);
.....
.....
.....
<p>My birthday is {{ myBirthday | date }}</p>
}
```

The code above turns the raw string "Thu Jun 9 1994 00:00:00 GMT-0700 (Pacific Daylight Time)" into June 9, 1994.

Along with this raw data conversion, format types are available, such as 'shortDate' and 'fullDate'.

[<-back home](../)




[<-previous](../dependencyinjection) |
[home](../) |
[next->](../routing)
