import { Injectable } from '@angular/core';

import { Topic } from './topic';
import { TOPICS } from './tutorial-topics';

@Injectable()
export class TopicService {

    getTopics(): Promise<Topic[]> {
        return Promise.resolve(TOPICS);
    }

    getTopic(id: number): Promise<Topic> {
        return this.getTopics()
            .then(topics => topics.find(topic => topic.id === id));
    }

}
