import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  template: `
        <h1>{{title}}</h1>
        <nav>
            <a routerLink="/dashboard">Dashboard</a>
            <a routerLink="/topics">Topics</a>
        <router-outlet></router-outlet>
    `,
    styleUrls: ['./css/app.component.css']
})

export class AppComponent {
  title = 'Angular 2 Tutorial';
}
