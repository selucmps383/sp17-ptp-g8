import { Topic } from './topic';

export const TOPICS: Topic[] = [
  {id: 1, name: 'Angular Overview', summary: 'General information'},
  {id: 2, name: 'TypeScript', summary: 'Superset of JavaScript'},
  {id: 3, name: 'Modules', summary: 'Modules are good'},
  {id: 4, name: 'Data Binding', summary: 'Binding data'},
  {id: 5, name: 'Components', summary: 'Basis'},
  {id: 6, name: 'Dependency Injection', summary: 'Much Dependent'},
  {id: 7, name: 'Routing', summary: 'Where you wanna go...'},
  {id: 8, name: 'Pipes', summary: 'Like a template filter'},
];
