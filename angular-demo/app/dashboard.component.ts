import { Component, OnInit} from '@angular/core';

import { Topic } from './topic';
import { TopicService } from './topic.service';

@Component({
    moduleId: module.id,
    selector: 'my-dashboard',
    templateUrl: './templates/dashboard.component.html',
    styleUrls: ['./css/dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
    topics: Topic[] = [];
    constructor(private topicService: TopicService) { }
    ngOnInit(): void {
        this.topicService.getTopics()
            .then(topics => this.topics = topics.slice(4,9));
    }
}

///////////////////////////////////////////////////////////////////////
//
//    Example of using the TopicService without dependency injection.
//    This would create a new instance of TopicService, as opposed
//    to just using it as a service. 
//
//    private topicService: TopicService;
//
//    constructor() {
//        this.topicService = new TopicService();
//    }
//
///////////////////////////////////////////////////////////////////////
