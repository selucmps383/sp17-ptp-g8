import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { Topic } from './topic';
import { TopicService } from './topic.service';

@Component({
    moduleId: module.id,
    selector: 'topic-detail',
    templateUrl: './templates/topic-detail.component.html',
    styleUrls: ['./css/topic-detail.component.css'],
})
export class TopicDetailComponent implements OnInit {
    @Input() topic: Topic;
    constructor(
        private topicService: TopicService,
        private route: ActivatedRoute,
        private location: Location
    ) {}

    ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => this.topicService.getTopic(+params['id']))
            .subscribe(topic => this.topic = topic);
    }

    goBack(): void {
        this.location.back();
    }
}
