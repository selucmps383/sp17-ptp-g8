import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Topic } from './topic';
import { TopicService } from './topic.service';

@Component({
  moduleId: module.id,
  selector: 'topics',
  templateUrl: './templates/topics.component.html',
  styleUrls: ['./css/app.component.css', './css/topics.component.css'],
  providers: [TopicService],
})
export class TopicsComponent implements OnInit { 
  title = 'Topics';
  topics: Topic[];
  selectedTopic: Topic;

  constructor(
    private router: Router,
    private topicService: TopicService) { }

  getTopics(): void {
    this.topicService.getTopics().then(topics => this.topics = topics);
  }

  ngOnInit(): void {
    this.getTopics();
  }

  onSelect(topic: Topic): void {
    this.selectedTopic = topic;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedTopic.id]);
  }
}
