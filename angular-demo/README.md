##_Note: this is an in-depth demo app based on the [tutorial](https://angular.io/docs/ts/latest/tutorial/) on the Angular site._
##_Look at the [Angular Quickstart repo](https://github.com/angular/quickstart) for the original source code._

Aside from some customization of the topics and file structure for `html` and `css` files, this basically follows the [Tour of Heroes](https://angular.io/docs/ts/latest/tutorial/) tutorial on the Angular website.
