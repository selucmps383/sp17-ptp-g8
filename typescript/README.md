# TypeScript


What is TypeScript?

Typescript is a superset of JavaScript. It provides more object-oriented programming concepts to create a more 
powerful and organized code structure. Typescript brings many improvements on JavaScript, but does not completely 
abandon the JavaScript language. JavaScript will compile in any .ts(TypeScript) file and run correctly.

Along with this object-oriented focus, variables in TypeScript posses a specific Datatype(string, boolean, ect) 
instead of using the dynamic var type. This creates a more strict codebase, but also a codebase that is less prone 
to defects, as stated in this [study of programming languages and code quality] done by the University of California. 

![datatypes](http://i66.tinypic.com/67l3cj.png)

Because Typescript is compiled, it has the ability to detect errors while coding. In JavaScript, an error can result 
in a long manual search through each line of code after run-time. With Typescript, errors are exposed immediately, 
and a possible solution is suggested as well.

![error](http://i68.tinypic.com/212vced.png)

[study of programming languages and code quality]:<http://web.cs.ucdavis.edu/~filkov/papers/lang_github.pdf>


[<-previous](../angular) |
[home](../) |
[next->](../modules)
