# What is Angular?

What Is Angular?

Angular 2 is a rewrite of AngularJS, a JavaScript framework used to create web applications. Instead of being an MVC framework, Angular 2 is component based. Angular 2 uses TypeScript as it's language, a superset of JavaScript. Angular 2 improves on AngularJS by focusing on mobile oriented architecture and mobile performance.

Angular 2 does code generation and templates, and is capable of code splitting when using the Component Router. This makes it so users only load the code required to render the current view.  Angular 2 works very well with the Ionic Framework. The Angular CLI is also available as command line tools for quick deployment.


Requirements:

* [Node.js][1]

* [NPM][2]

* [Angular 2 Documentation][3]

[1]: https://nodejs.org/en/
[2]: https://www.sitepoint.com/beginners-guide-node-package-manager/
[3]: https://angular.io/docs/ts/latest/


[<-back home](../) |
[next->](../typescript)
