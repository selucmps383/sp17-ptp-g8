# Data Binding

Synchronizating the data model and the view of an application is called Data Binding. This two-way binding ensures that data displayed on the view of an application is true to what is kept in the data model. When data in the model changes, the view changes. And when the view changes, the model changes. An example of Data Binding is here:

```javascript

var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope) {

    $scope.firstname = "John";

    $scope.lastname = "Doe";

});

```

```html

<p>First name: {{firstname}}</p>

```

The first example is our data model, kept in the backend. In $scope is the variable 'firstname', which we will refer to later in our HTML, using double brackets. This allows the HTML file to use our variables, and reflect changes made in the data model.

[<-previous](../modules) |
[home](../) |
[next->](../components)
