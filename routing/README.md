# Routing in Angular

## What is a route?
>"A route is a URL pattern that is mapped to a handler. The handler can be a physical file, such as an .aspx file in a Web Forms application. A handler can also be a class that processes the request, such as a controller in an MVC application."
>--[Microsoft MSDN Website](https://msdn.microsoft.com/en-us/library/cc668201.aspx)

## Basic Routing

### Base Tag
The first step of routing is to set the ```<base href>``` tag. For instance, if the root of your application is in the ```app``` directory, you would set the base tag in ```index.html``` like this: 
```html
<base href="/">
```
This sets the ```app``` directory as the route to be used when the application is determining routes. This also tells the browser how to prefix relative URLs when importing assets.

Read more about the [```<base href>```](https://angular.io/docs/ts/latest/guide/router.html#!#base-href) tag and [```history.pushState()```](https://developer.mozilla.org/en-US/docs/Web/API/History_API#Adding_and_modifying_history_entries) for a more comprehensive explanation of how this works.

### Defining Routes

The most basic way to define routes is in the ```AppModule```. Start by importing the Angular ```RouterModule``` in ```app.module.ts```, which looks like this:

```typescript
import { RouterModule }   from '@angular/router';
```
Once imported, the ```RouterModule``` can be used to add configuration to imports array of ```app.module.ts```.

```typescript
RouterModule.forRoot([
  { path: 'greet', component: GreetComponent }
])
```
This configuration would route a URL like ```example.com/greet``` to a component called ```GreetComponent``` which would then handle that request. To add another route, define another path and component, then add that to the array. 

```typescript
RouterModule.forRoot([
  { path: 'greet', component: GreetComponent },
  { path: 'goodbye', component: GoodbyeComponent }
])
```
A request to ```example.com/goodbye``` would then be handled by the ```GoodbyeComponent```.

_Note: The components will also have to be imported to_ ```app.module```.

### Routing and Views
Routers display views using the ```RouterOutlet``` directive from the ```RouterModule```. This gives us the tag ```<router-outlet>``` which is used as a DOM element in the template of the component the router is mapped to.

```typescript
template:`
    <h1>Title</h1>
    <router-outlet></router-outlet>
    <!-- view would be displayed here in the browser -->
```
### Routing and Links

Using ```RouterLink```, another directive of ```RouterModule```, routes can be bound to an ```<a>``` tag so that routing can be used with links. This also goes in the template of a component.

```typescript
template:`
    <h1>Title</h1>
    <a routerLink="/greet">Greet</a>
`
```
Clicking on the ```Greet``` link would send the request through the 'greet' route to the ```GreetComponent```.

## Advanced Routing

* redirectTo
* route with parameters
* Separating routes into their own file.


## Resources

* [Angular Docs](https://angular.io/docs/ts/latest/guide/router.html "Routing and Navigation")
* [angular-university](http://blog.angular-university.io/angular2-router/ "Angular 2 Router Introduction")
* [vsavkin.com Angular Blog](https://vsavkin.com/angular-2-router-d9e30599f9ea#.2zn84h6pk)

[<-previous](../pipes) 
[home](../)
[angular demo->](../angular-demo)
