# Components

Components are what everything is made of in Angular 2. They are the main way to build and specify elements and logic on a page, and contain custom elements and attributes. Marking something as a component provides additional metadata as to how to process and use the component. Components are subset of directives in Angular, and hold a template. They must belong to NgModule in order to be usable. An example of a component is here:

```typescript

import { Component } from '@angular/core';

 
@Component({
  selector: 'greet',
  template: `<h1>Hello {{name}}</h1>`,
})
export class GreetComponent  { name = 'World'; }

```

This component is given the selector of 'greet' and the template of 'Hello {{name}}!'. The class named as a component is Greet and the name used in the template is a string equal to 'World'.


[<-previous](../databinding) | 
[home](../) | 
[next->](../dependencyinjection)
