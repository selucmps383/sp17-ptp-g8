# Modules

Modules are the containers for both code and declarations. Modules are designed to serve a specific purpose, and are written to be interchangeable.
Modules posses their own scope, meaning that the code within cannot be executed globally unless they are given an export form. To access a module, it must be imported.

The NgModule is the "Angular Module". Its purpose is to tell Angular how to run the module code. It also will make some components of the modules public so that they can be used elsewhere.

![](http://i66.tinypic.com/xdgg06.png)

[<-previous](../typescript) |
[home](../) |
[next->](../databinding)
